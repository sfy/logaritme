package com.sfy.logaritme;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.ListView;

import net.sourceforge.zbar.Symbol;

import qrscanner.ScanCodeActivity;

public class ReciboPalet extends Activity {

    private static final String TAG = "ReciboPalet";
    private static final int SCAN_SSCC = 1;

    Button escanPaletBut;
    ListView packingList;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_recibo_palet);

        escanPaletBut = (Button) findViewById(R.id.escanPaletBut);
        packingList = (ListView) findViewById(R.id.packingList);

        escanPaletBut.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Log.i(TAG, "Escanning SSCC");
                escanearSSCC();
            }
        });

    }

    private void escanearSSCC() {
        Intent intent = new Intent(ReciboPalet.this, ScanCodeActivity.class);
        intent.putExtra(ScanCodeActivity.SCAN_MODES, new int[] {Symbol.CODE128});
        startActivityForResult(intent, SCAN_SSCC);
    }



    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == SCAN_SSCC) {
            if (resultCode == RESULT_OK) {
                String scan = data.getStringExtra(ScanCodeActivity.SCAN_RESULT);
                Log.i(TAG, "Scanned palet "+scan);
                if ("00".equals(scan.substring(0,2))) {

                }
                else {
                    Log.e(TAG,"Palet no válido");
                }
            }
            else {
                
            }
         }
        super.onActivityResult(requestCode, resultCode, data);
    }
    
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.recibo_palet, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();
        if (id == R.id.action_settings) {
            return true;
        }
        return super.onOptionsItemSelected(item);
    }
}
