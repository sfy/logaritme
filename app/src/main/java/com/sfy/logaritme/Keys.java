package com.sfy.logaritme;

import android.os.Environment;

/**
 * Created by programacion1 on 13/08/14.
 */
public class Keys {
    public static final String SRC_WS_URL = "http://onair.sfy.com/logistica";
    public static final String APP_DIR = Environment.getExternalStorageDirectory().getAbsolutePath()+"/Logaritme";
    public static final String URL_PHOTO_VERIFY = SRC_WS_URL+"";// Afegir el path pel WS de foto
    public static final String INTENT_EXTRA_LOCATION = "ubicacion";

}
