package com.sfy.logaritme;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.speech.RecognizerIntent;
import android.speech.SpeechRecognizer;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.loopj.android.http.AsyncHttpClient;
import com.loopj.android.http.AsyncHttpResponseHandler;

import net.sourceforge.zbar.Symbol;

import org.apache.http.Header;
import org.json.JSONException;
import org.json.JSONObject;


import java.io.File;

import qrscanner.ScanCodeActivity;


public class MainAcitvity extends Activity {

    private static final String TAG = "MainActivity";
    private static final int CHECK_PACKAGE = 1;
    private static final int DOUBLE_CHECK = 2;
    private String mUbicacion = "";
    private String mPaquete = "";

    SpeechRecognizer sr;

    TextView label,value;
    ListView prova;
    Button packCheckButton,locationCheckButton;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main_acitvity);

        File faux = new File(Keys.APP_DIR);
        if (!faux.exists())
            faux.mkdirs();

        /*prova = (ListView) findViewById(R.id.myList);
        prova.setAdapter(new ArrayAdapter<String>(getApplicationContext(),android.R.layout.simple_list_item_1, new String[] {"A","B","C","D","E","F"}));
        prova.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                Toast.makeText(getApplicationContext(), prova.getAdapter().getItem(i).toString(), Toast.LENGTH_SHORT).show();
            }
        });*/

        packCheckButton = (Button) findViewById(R.id.escanPackBut);
        locationCheckButton = (Button) findViewById(R.id.doubleCheckBut);
        locationCheckButton.setVisibility(View.INVISIBLE);
        label = (TextView) findViewById(R.id.label_text);
        value = (TextView) findViewById(R.id.value_text);

        packCheckButton.bringToFront();
        locationCheckButton.bringToFront();
        packCheckButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                scanBarPaquet();
            }
        });

        locationCheckButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                scanBarDoubleCheck();
            }
        });
        packCheckButton.requestFocus();

    }

    private void scanBarPaquet() {
        Log.i(TAG,"CHECK PACK");
        Intent intent = new Intent(this, ScanCodeActivity.class);
        intent.putExtra(ScanCodeActivity.SCAN_MODES, new int[]{Symbol.CODE128});
        startActivityForResult(intent, CHECK_PACKAGE);
    }

    private void fesPeticio(String paquetId) {
        mPaquete = paquetId;
        AsyncHttpClient client = new AsyncHttpClient();
        final String laurl = Keys.SRC_WS_URL+"/api/lecturapaquet?codibarres="+paquetId;

        client.get(laurl, new AsyncHttpResponseHandler() {

            @Override
            public void onStart() {
                // called before request is started
            }

            @Override
            public void onSuccess(int statusCode, Header[] headers, byte[] response) {
                // called when response HTTP status is "200 OK"
                Log.d(TAG,new String(response));
                try {
                    JSONObject json = new JSONObject(new String(response));
                    if (json.has("resultat")) {
                        if (json.getInt("resultat") == 200) {
                            String codiBarresUbicacio = json.getString("codibarresdesti");
                            int x1 = json.getInt("x1");
                            int x2 = json.getInt("x2");
                            int y1 = json.getInt("y1");
                            int y2 = json.getInt("y2");
                            Log.i(TAG, "JSON resultat:  CodiBarresDesti -> " + codiBarresUbicacio + " x1 -> " + x1 + " x2 -> " + x2 + " y1 -> " + y1 + " y2 -> " + y2);
                            packCheckButton.setVisibility(View.GONE);
                            locationCheckButton.setVisibility(View.VISIBLE);
                            locationCheckButton.requestFocus();
                            mUbicacion = codiBarresUbicacio;
                            label.setText(getString(R.string.muelle_destino));
                            value.setText(codiBarresUbicacio);
                        }
                        else if (json.getInt("resultat") == 500) {
                            Log.e(TAG,"Motiu error: "+json.getString("motiu"));
                            label.setText(getString(R.string.error));
                            value.setText(json.getString("motiu"));
                        }
                    }
                } catch (JSONException e) {
                    Log.i("Una altra web", new String(response ));
                    e.printStackTrace();
                }
            }

            @Override
            public void onFailure(int statusCode, Header[] headers, byte[] errorResponse, Throwable e) {
                // called when response HTTP status is "4XX" (eg. 401, 403, 404)
                Log.i(TAG,"onFailure "+statusCode+" "+laurl);
                label.setText(getString(R.string.error));
                value.setText(getString(R.string.connectionFailure));
            }

            @Override
            public void onRetry(int retryNo) {
                // called when request is retried
            }
        });
    }

    private void doubleCheck(String ubicId) {
        if (!"".equals(mUbicacion) && mUbicacion.equals(ubicId)) {
            label.setText(getString(R.string.double_check_ok));
            value.setText("En muelle: "+ubicId);
            mUbicacion = "";
            packCheckButton.setVisibility(View.VISIBLE);
            locationCheckButton.setVisibility(View.INVISIBLE);
        }
        else {
            label.setText(getString(R.string.error_double_check));

        }
    }


    private void scanBarDoubleCheck() {
        Intent intent = new Intent(this, ScanCodeActivity.class);
        intent.putExtra(ScanCodeActivity.SCAN_MODES, new int[]{Symbol.CODE128});
        startActivityForResult(intent,DOUBLE_CHECK);

    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == CHECK_PACKAGE) {
            if (resultCode == RESULT_OK) {
                String paquetId = data.getStringExtra("SCAN_RESULT");
                fesPeticio(paquetId);
                Log.i(TAG, "EScannejat paquet: " + paquetId);

            }
        } else if (requestCode == DOUBLE_CHECK) {
            if (resultCode == RESULT_OK) {
                String ubicacioId = data.getStringExtra("SCAN_RESULT");
                Log.i(TAG, "EScannejat ubicació: " + ubicacioId);
                doubleCheck(ubicacioId);
            }
        }
    }
}
