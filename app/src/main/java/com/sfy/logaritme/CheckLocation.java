package com.sfy.logaritme;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import com.sfy.logaritme.R;

import net.sourceforge.zbar.Symbol;

import qrscanner.ScanCodeActivity;

public class CheckLocation extends Activity {

    private static final int DOUBLE_CHECK = 1;
    private static final String TAG = "CheckLocation";
    private String mUbicacion = "";
    TextView label,value;
    Button checkBut;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_check_location);

        Bundle b = getIntent().getExtras();
        if (b.containsKey(Keys.INTENT_EXTRA_LOCATION)) {
            mUbicacion = b.getString(Keys.INTENT_EXTRA_LOCATION);
        }

        label = (TextView) findViewById(R.id.label_text_location);
        value = (TextView) findViewById(R.id.value_text_location);
        checkBut = (Button) findViewById(R.id.checkLocationBut);

        checkBut.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                scanBarDoubleCheck();
            }
        });
    }

    private void scanBarDoubleCheck() {
        Intent intent = new Intent(this, ScanCodeActivity.class);
        intent.putExtra(ScanCodeActivity.SCAN_MODES, new int[]{Symbol.CODE128});
        startActivityForResult(intent,DOUBLE_CHECK);

    }

    private void doubleCheck(String ubicId) {
        if (!"".equals(mUbicacion) && mUbicacion.equals(ubicId)) {
            label.setText(getString(R.string.double_check_ok));
            value.setText("En muelle: "+ubicId);
            mUbicacion = "";
            checkBut.setVisibility(View.INVISIBLE);
        }
        else {
            label.setText(getString(R.string.error_double_check));

        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == DOUBLE_CHECK) {
            if (resultCode == RESULT_OK) {
                String ubicacioId = data.getStringExtra("SCAN_RESULT");
                Log.i(TAG, "EScannejat ubicació: " + ubicacioId);
                doubleCheck(ubicacioId);
            }
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.check_location, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();
        if (id == R.id.action_settings) {
            return true;
        }
        return super.onOptionsItemSelected(item);
    }
}
