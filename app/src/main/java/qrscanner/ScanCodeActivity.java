package qrscanner;

import java.util.ArrayList;

import net.sourceforge.zbar.Config;
import net.sourceforge.zbar.Image;
import net.sourceforge.zbar.ImageScanner;
import net.sourceforge.zbar.Symbol;
import net.sourceforge.zbar.SymbolSet;
import android.app.Activity;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.hardware.Camera;
import android.hardware.Camera.AutoFocusCallback;
import android.hardware.Camera.Parameters;
import android.hardware.Camera.PreviewCallback;
import android.hardware.Camera.Size;
import android.os.Bundle;
import android.os.Handler;
import android.text.TextUtils;
import android.util.Log;
import android.view.WindowManager;
import android.widget.FrameLayout;

import com.sfy.logaritme.R;

public class ScanCodeActivity extends Activity {
	private static final String TAG = "ScanQRActivity";
	private Camera mCamera;
	private CameraPreview mPreview;
	private Handler autoFocusHandler;
	private ImageScanner mScanner;
	
	public static String SCAN_MODES = "SCAN_MODES";
	public static String SCAN_RESULT = "SCAN_RESULT";
	public static String SCAN_RESULT_TYPE = "SCAN_RESULT_TYPE";
	public static String ERROR = "ERROR";

    private boolean mPreviewing = true;

	static {
		System.loadLibrary("iconv");
	}

	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		Log.i(TAG, "ON CREATE");
		setContentView(R.layout.zbartest);
		this.getWindow().addFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);

	    if(!isCameraAvailable()) {
	        // Cancel request if there is no rear-facing camera.
	        cancelRequest();
	        return;
	    }
	      
		autoFocusHandler = new Handler();

		setupScanner();

		mPreview = new CameraPreview(this, mCamera, previewCb, autoFocusCB);
		FrameLayout preview = (FrameLayout) findViewById(R.id.cameraPreview);
		preview.addView(mPreview);

	}

	public boolean isCameraAvailable() {
        PackageManager pm = getPackageManager();
        return pm.hasSystemFeature(PackageManager.FEATURE_CAMERA);
	}
	   
	@Override
	protected void onResume() {
		super.onResume();
		Log.i(TAG, "ON RESUME");
		// Open the default i.e. the first rear facing camera.
        mCamera = Camera.open();
        Parameters param = mCamera.getParameters();
        ArrayList<Size> size = (ArrayList<Size>) param.getSupportedPreviewSizes();
        Log.i(TAG,"PreviewSize: "+size.get(0).width+"x"+size.get(0).height);
        param.setPreviewSize(size.get(0).width, size.get(0).height);
        mCamera.setParameters(param);
        
        //Google glass error fixing
        try {
        	//googleGlassCameraGlitchWorkAround(mCamera);
        }
        catch (Exception e) {
        	Log.e(TAG,"failed to set parameters");
        	e.printStackTrace();
        }
        
        if(mCamera == null) {
        	Log.e(TAG, "null camera");
            // Cancel request if mCamera is null.
            cancelRequest();
            return;
        }

        mPreview.setCamera(mCamera);
        mPreviewing = true;
	}
	
	public void googleGlassCameraGlitchWorkAround(Camera mCamera) {
        Parameters params = mCamera.getParameters();
        params.setPreviewFpsRange(30000, 30000);
        mCamera.setParameters(params);
	}

	@Override
	public void onPause() {
		super.onPause();
		Log.i(TAG, "ON PAUSE");
		if (mCamera != null) {
			releaseCamera();
		}
	}

	private void releaseCamera() {
		if (mCamera != null) {
		
		       mPreview.setCamera(null);
	            mCamera.cancelAutoFocus();
	            mCamera.setPreviewCallback(null);
	            mCamera.stopPreview();
	            mCamera.release();

	            mPreviewing = false;
	            mCamera = null;
	            
		}
	}

	private Runnable doAutoFocus = new Runnable() {
		public void run() {
			if (mPreviewing && mCamera!=null)
				mCamera.autoFocus(autoFocusCB);
		}
	};

	PreviewCallback previewCb = new PreviewCallback() {
		public int count = 0;
		
		public void onPreviewFrame(byte[] data, Camera camera) {
			if (count == 2) {
				count = 0;
				Parameters parameters = camera.getParameters();
				Size size = parameters.getPreviewSize();
	
				Image barcode = new Image(size.width, size.height, "Y800");
				barcode.setData(data);
	
				int result = mScanner.scanImage(barcode);
	
				if (result != 0) {
					mCamera.cancelAutoFocus();
					mCamera.setPreviewCallback(null);
					mCamera.stopPreview();
					mPreviewing = false;
					SymbolSet syms = mScanner.getResults();
					for (Symbol sym : syms) {
						String symData = sym.getData();
						if (!TextUtils.isEmpty(symData)) {
							Intent dataIntent = new Intent();
							dataIntent.putExtra(SCAN_RESULT, symData);
							dataIntent.putExtra(SCAN_RESULT_TYPE, sym.getType());
							setResult(Activity.RESULT_OK, dataIntent);
							finish();
							break;
						}
					}
				}
			}
			else ++count;
		}
	};

	// Mimic continuous auto-focusing
	AutoFocusCallback autoFocusCB = new AutoFocusCallback() {
		public void onAutoFocus(boolean success, Camera camera) {
			autoFocusHandler.postDelayed(doAutoFocus, 1000);
		}
	};

	public void cancelRequest() {
		Intent dataIntent = new Intent();
		dataIntent.putExtra(ERROR, "Camera unavailable");
		setResult(Activity.RESULT_CANCELED, dataIntent);
		finish();
	}

	public void setupScanner() {
		mScanner = new ImageScanner();
		mScanner.setConfig(0, Config.X_DENSITY, 3);
		mScanner.setConfig(0, Config.Y_DENSITY, 3);

		int[] symbols = getIntent().getIntArrayExtra(SCAN_MODES);
		if (symbols != null) {
			mScanner.setConfig(Symbol.NONE, Config.ENABLE, 0);
			for (int symbol : symbols) {
				mScanner.setConfig(symbol, Config.ENABLE, 1);
			}
		}
	}

}
